__author__ = 'kohoutekl'

# HTML page template for Mailer.


pageTemplate = '''
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator" content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

  <title>Password Reminder</title>
</head>

<body style="background-color:#FAFAFA" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
  <center>
    <table style="background-color:#FAFAFA" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id=
    "backgroundTable">
      <tr>
        <td align="center" valign="top">
          <table style="border: 1px solid #DDDDDD;background-color:#FFFFFF;" border="0" cellpadding="0" cellspacing="0" width="800"
          id="templateContainer">
            <tr>
              <td align="center" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="800" height="69" id="templateHeader">
                  <tr>
                    <td class="headerContent"> <!-- <img src="file:///E:/tools/header.png" style="max-width:800px;" /> --> <h1 style=
                              "color:#202020;display:block;font-family:Arial;font-size:34px;font-weight:bold;line-height:100%;margin-top:10px0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:center;"
                              class="h1">Network Password Reminder</h1></td>
                  </tr>
                </table>
              </td>
            </tr>

            <tr>
              <td align="center" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="800" id="templateBody">
                  <tr>
                    <td valign="top" class="bodyContent">
                      <table border="0" cellpadding="20" cellspacing="0" width="100%">
                        <tr>
                          <td valign="top">
                            <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left;" mc:edit=
                            "std_content00">
                              <!-- <h1 style=
                              "color:#202020;display:block;font-family:Arial;font-size:34px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:center;"
                              class="h1">Network Password Reminder</h1> --> <!-- <h2 class="h2">Heading 2</h2> -->

                              <h3 style=
                              "color:#202020;font-family:Arial;font-size:26px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;"
                              class="h3">Dear {person},</h3><br />

                              <h4 style=
                              "color:#202020;font-family:Arial;font-size:22px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;"
                              class="h4">Your network password will expire on:{exp_date}</h4><br />
                              <strong style="color:red;">Recommended Action:</strong> Please change your network password.<br />

                              <h4>Passwords must meet the following complexity requirements:</h4>

                              <ul>
                                <li>Minimum password length of 8 characters</li>

                                <li>Must contains 3 of the following 4 following categories:</li>

                                <li>Uppercase characters (A - Z)</li>

                                <li>Lowercase characters (a - z)</li>

                                <li>Numeric digits (0-9)</li>

                                <li>Non-alphanumeric (For example: !, $, #, or %)</li>
                              </ul>

                              <p>Please email <a href="mailto:foo@bar.com?subject=Password%20Change%20Assistance">foo@bar.com</a>, or call the Servicedesk if you have any questions or need assistance in changing your password.<br />
                              <br />
                              
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

            <tr>
              <td align="center" valign="top">
                <table border="0" cellpadding="10" cellspacing="0" width="800" id="templateFooter">
                  <tr>
                    <td style=
                    "background-color:#FFFFFF;border:0;text-align:center;color:#707070;font-family:Arial;font-size:12px;line-height:125%;"
                    colspan="2" valign="middle" id="utility">
                      <center>
                        <div mc:edit="std_utility">
                          <a href="mailto:foo@bar.com?subject=Password%20Change%20Assistance">Email the
                          Servicedesk</a> | <a href="https://servicedesk" target=
                          "_blank">Visit the Servicedesk Portal</a>
                        </div>
                      </center>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table><br />
        </td>
      </tr>
    </table>
  </center>
</body>
</html>


'''


pageTemplate_twoweeks = '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Password Reminder</title>
</head>
<body style="background-color:#FAFAFA" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
<table border="1" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="800" id="templateContainer">
<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="800" id="templateHeader">
<tr>
<td class="headerContent">
<img src="" style="max-width:800px;"/>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="800" id="templateBody">
<tr>
<td valign="top" class="bodyContent">
<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
<td valign="top">
<div mc:edit="std_content00">
<h1 class="h1">Network Password Reminder</h1>
<!-- <h2 class="h2">Heading 2</h2> -->
<h3 class="h3">Dear {person},</h3>
<h4 class="h4">Your network password will expire on:{exp_date}</h4>
<strong>Recommended Action:</strong> Please change your password in the next two weeks.
<br />
<br />
Here's where we would insert a link to internal KB on how to change password <a href="http://technet.microsoft.com/en-us/library/cc747350%28v=ws.10%29.aspx" target="_blank"> with the link text is here</a>. Something something, call or email the servicedesk for assistance.
<br />
<br />
More information, etc..etc...
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">
<table border="0" cellpadding="10" cellspacing="0" width="800" id="templateFooter">
<tr>
<td valign="top" class="footerContent">
</td>
</tr>
</table>
</td>
</tr>
</table>
<br />
</td>
</tr>
</table>
</center>
</body>
</html>
'''

# Here is super simple html for testing.

pt_two = '''
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta content="text/html; charset=ISO-8859-1"
 http-equiv="content-type">
  <title>Hello</title>
</head>
<body>
Hello, {person} lalala {exp_date}!
</body>
</html>'''