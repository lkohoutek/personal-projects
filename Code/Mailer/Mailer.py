#!/usr/bin/env python
#title           : Mailer.py
#description     : Password Reminder Email thing..
#author          : Levon Kohoutek | lkohoutek@gmail.com
#date            : 20150302
#version         : 0.1.1
#usage           : python Mailer.py
#python_version  : 3.4
#notes           :
#
#
#
#==============================================================================

import os
import glob
import csv
import logging
from datetime import *
import configparser
config = configparser.ConfigParser()
from subprocess import call
# =========
import smtplib
import email.utils
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
import time

# Set date and time variables
current_day = time.strftime("%d")
current_day_day = time.strftime("%a")
current_year = time.strftime("%Y")
current_time = time.strftime("%H:%M")



# Change the dir - slap this in the config yo!
os.chdir("G:\\temp")

# Set some static DIR's TODO: Put this junk in a config file man.
r_dir = "G:\\temp"
ht_dir = "G:\\temp\\html"



# Logging Stuff
# We need to add a flag to set how verbose we want to log.
logging.basicConfig(filename='Mailer.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.info('>>>=== Mailer 0.1.1! | Mar 3 2015 | lkohoutek@gmail.com              ===<<< ')
logger.info('>>>=== ------------------------------------------------------------  ===<<< ')

# Open the CSV - slap this in the config, yo!
# logger.info(csv_file_name)
csv_file_name_raw = "IT_minority_report.csv"
csv_file_name = "fixed.csv"

# Get file create date
born_on = time.ctime(os.path.getmtime(csv_file_name_raw))
logger.info(born_on)

# Adding our testing CSV
#csv_file_name_raw = "IT_minority_report_test.csv"
#csv_file_name = "fixed_test.csv"

logger.info("Watch the lady...")
# Here we gank that first line out. There is a powershell solution for this.
file_in = open(csv_file_name_raw, "r")  # This opens the raw CSV
file_data = file_in.readlines()  # This loads this variable up with the lines
file_in.close()
del file_data[0]  # No more first line
file_out = open(csv_file_name, "w")  # Barf it back
file_out.writelines(file_data)  # Push it in
file_out.close()  # walk away

# Load the fixed CSV
c = csv.reader(open(csv_file_name, 'rt'))

# Create lists for our rows
given_name = []
samAccountName = []
mail = []
password_status = []
password_expires = []
account_is_disabled = []
account_is_locked_out = []
password_is_expired = []

# Libs
html_output = []
expired_users = [] # Put users who are expired in this one.
total_users = []
total_exping_users = []

# Numberwang
# i is used in our sanity check
i = 0
# ii is used in generate_html_body as the iterator.
ii = 0

# Import the HTML code from Mailer_pagetemplate.py
from CCGmailer_pagetemplate import pageTemplate


# /===========================================FUNCTIONS START==========================================================\

def sanity_check(given_name, samAccountName, mail, password_status, password_expires, account_is_disabled,
                 account_is_locked_out, password_is_expired, i):
    """
    On script load, it parses out the CSV in to dict's. This goes through and pulls the row from the associated
    dicts, and logs them to the text file. This should be disabled in production, and re-enabled for troubleshooting.
    :param given_name:
    :param samAccountName:
    :param mail:
    :param password_status:
    :param password_expires:
    :param account_is_disabled:
    :param account_is_locked_out:
    :param password_is_expired:
    :param i: Our iterator
    :return:
    """
    try:
        logger.info("Running Sanity Check - Disable in production.")
        for r in given_name:
            logger.info(given_name[i] + " " + samAccountName[i] + " " +  mail[i] + " "  + password_status[i] + " "  +
                        password_expires[i] + " "  + account_is_disabled[i] + " "  + account_is_locked_out[i] + " "  +
                        password_is_expired[i])
            i +=1
    except:
        logger.info("sanity check failed!")
        pass

def generate_html_body_and_send(given_name,password_status,ii):
    """
    Jeez, this one does it all, unfortunately.

    Take the rows in our table, and go through them 1 by one.

    It sorts out users, and users that meet a set criteria get html email bodys generated.

    :param given_name: No given name, no email.
    :param password_status: If there's a non/null it will make this func puke.
    :param ii: Plain ole iterator.
    :return:
    """
    logger.info("==> Creating HTML <==")
    try:
        for r in given_name:
            person = given_name[ii]
            try:
                exp_date = password_status[ii].split(":")[1] # lol this fails if status is expired - show stopper
            except:
                exp_date = " Sorry, your password has expired."
                expired_users.append(samAccountName[ii])
                logger.info("Password Has expired")
                logger.info("Curent Naughty Users: " + str(expired_users))
                # logger.info(expired_users)
            # Time stuff
            # http://stackoverflow.com/questions/3278999/how-can-i-compare-a-date-and-a-datetime-in-python
            from_date = date.today()+timedelta(days=7)
            from_date_two = date.today()+timedelta(days=7)
            pwd_date = password_expires[ii].split(" ")[0]
            password_date = datetime.strptime(pwd_date, "%m/%d/%Y")
            # password_date = datetime.date(2014, 9, 14) & from_date_two = datetime.date(2014, 8, 28)
            # password date is the date the password expires.
            # from_date_two is Today, plus 14 days.
            logger.info("==> Starting password logic loop <==")
            if password_date.date() < from_date_two:
                if password_date.date() < from_date:
                    os.chdir(ht_dir)
                    # If the users password expires in 7 days or less, do the following.
                    # Note, we can separate out the functions so 14/7 day emails are different.
                    contents = pageTemplate.format(**locals())
                    # contents = pt_two.format(**locals())
                    # here's where we need to write it out
                    html_output.append(contents)
                    filename = samAccountName[ii] + ".html"
                    # logger.info(filename)
                    output = open(filename, "w")
                    output.write(contents)
                    output.close()
                    # logger.info(samAccountName)
                    # logger.info("==> Wrote HTML <==")
                    # logger.info(ii)
                    env_lope = samAccountName[ii] + "@bar.com"
                    a_slug = samAccountName[ii]
                    total_exping_users.append(samAccountName[ii])
                    open_fire(filename,env_lope)
                if password_date.date() > from_date:
                    os.chdir(ht_dir)
                    # Here is where you can set a 14 day or so HTML.
                    # contents = pageTemplate_twoweeks.format(**locals())
                    contents = pageTemplate.format(**locals())
                    # contents = pt_two.format(**locals())
                    # here's where we need to write it out
                    html_output.append(contents)
                    filename = samAccountName[ii] + ".html"
                    # logger.info(filename)
                    output = open(filename,"w")
                    output.write(contents)
                    output.close()
                    # logger.info(samAccountName)
                    # logger.info("==> Wrote HTML <==")
                    env_lope = samAccountName[ii] + "@bar.com"
                    open_fire(filename,env_lope)
                    total_exping_users.append(samAccountName[ii])
                    # logger.info(ii)
            else:
                # more stuff
                logger.info("==> Not 14 days <==")
                logger.info(samAccountName[ii])
                logger.info(ii)
            logger.info("test_html loop complete, incrementing integer")
            logger.info(ii)
            ii += 1
            logger.info(ii)
    except:
        logger.info("Outta Names!")
        logger.info(ii)
        pass


# This mofo sends it off.
def open_fire(bods,twos):
    """
    Assembles the commands to send the email with swithmail.

    TODO: Refactor this to dir the dir and grab the files. The html files are named the samaccountname,
    thats what would get emailed.
    so dir, read file name, use filename to build email.

    Also, slap this in the conf, yo! Look at all the redundant data.

    Check for check flag. but yah. Brill.

    :twos:
    :bods:
    :return:
    """
    try:
        logger.info("==> OPEN FIRE <==")
        os.chdir(ht_dir)
        exe = "SwithMail.exe"
        silent_s = "/s"
        from_s = "/from"
        from_add = '"foo@bar.com"'
        name_s = "/name"
        name = '"Password Reminder"'
        server_s = "/server"
        server = '"smtp"'
        to_s = "/to"
        #to = twos # Twos?
        to = '"foo@bar.com"'
        sub_s = "/sub"
        sub = '"Network Password Reminder"'
        btext_s = "/btxt"
        html_body = bods # Bods?
        html_s = "/html"
        logger.info(exe + " " + silent_s + " " +  from_s + " " + from_add + " " +  name_s + " " +  name + " " +
                    server_s + " " +  server + " " +  to_s + " " +  to + " " +  sub_s + " " +  sub + " " +  btext_s +
                    " " +  html_body + " " +  html_s)
        the_command = exe + " " + silent_s + " " +  from_s + " " + from_add + " " +  name_s + " " +  name + " " +\
                      server_s + " " +  server + " " +  to_s + " " +  to + " " +  sub_s + " " +  sub + " " +  btext_s +\
                      " " +  html_body + " " +  html_s
        # This works now - uncomment to call it. 
        call(the_command)
        print(the_command)
        # TODO: Write seperate log to send to servicedesk. 
    except:
        logger.error("Check Swith Dir Path")
        pass


# TODO: Refactor out Swithmail
def send_mail():
    try:
       logger.info("==> OPEN FIRE <==")
       os.chdir(ht_dir)
    except:
        pass


# Check for Powershell Completion Successful
def ps_completed_check():
    try:
        # logic to check for powershell completion
        # read the csv, get the date/time, is it today?
        bod = born_on.split(" ")
        file_date = (int(bod[2])+1)  # Python Script runs after midnight, CSV is generated the day before.
        todays_date = int(current_day)
        if file_date == todays_date:
            print("File is correct.")
            print(file_date)
            print(todays_date)
            print(bod[2])
            pass
        else:
            runtime_date = (current_year + " " + current_day + " " + current_day_day + " " + current_time)
            updated_body_text = ("""Password Email Report: {date}

            Data file born on date: {bod}

            """).format(date=runtime_date, bod=born_on)
            msg = MIMEMultipart()
            msg['To'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
            #msg['Cc'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
            msg['From'] = email.utils.formataddr(('Author', 'foo@bar.com'))
            msg['Subject'] = "Password Reminder Status Report"
            msg.attach(MIMEText(updated_body_text))
            server = smtplib.SMTP('smtp')
            # Uncomment below if relay is being strange:
            # server.set_debuglevel(True) # show communication with the server
            os.chdir("G:\\temp")
            f = "fixed.csv"
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(f, 'rb').read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="Password_Report.csv"')
            msg.attach(part)
            try:
                #server.sendmail('foo@bar.com', ['foo@bar.com'], msg.as_string())
                print("It worked!")
                print(file_date)
                print(todays_date)
                print(bod[2])
                pass
            finally:
                server.quit()
    except:
        raise Exception("Powershell task did not complete successfully.")


def clean_house():
    logger.info("We're deleting a bunch of files now!")
    os.chdir(ht_dir)
    filelist = glob.glob("*.html")
    for f in filelist:
        os.remove(f)
    os.chdir(r_dir)


def remove_fixed_csv():
    os.chdir(r_dir)
    file_out.close()
    os.remove(csv_file_name)


def send_report():
    """

    :return:
    """
    try:
        runtime_date = (current_year + " " + current_day + " " +
                        current_day_day + " " + current_time)
        updated_body_text = ("""Password Email Report: {date}

============= Password Reminder Email Status ===================

Total Users Checked: {tuc}

Total of users whose passwords expire in 14 days or less: {teu}

Total count of expired/locked out users: {tce}


============ The following users have expired passwords: =======

{expired_users}


===============================================================
Data file born on date: {bod}


This email was automatically generated by Mailer 0.1.1 ==
For Questions: foo@bar.com

        """).format(date=runtime_date, tuc=total_users_count, teu=teu_len, tce=texu_len,
                    expired_users=expired_users, bod=born_on)
        msg = MIMEMultipart()
        msg['To'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        msg['Cc'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        msg['From'] = email.utils.formataddr(('Author', 'foo@bar.com'))
        msg['Subject'] = "Password Reminder Status Report"
        msg.attach(MIMEText(updated_body_text))
        server = smtplib.SMTP('smtp')
        # Uncomment below if relay is being strange:
        # server.set_debuglevel(True) # show communication with the server
        os.chdir("G:\\tmep")
        f = "fixed.csv"
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(f, 'rb').read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="Password_Report.csv"')
        msg.attach(part)
        try:
            #server.sendmail('foo@bar.com', ['foo@bar.com'], msg.as_string())
            print
            pass
        finally:
            server.quit()
    except:
        print("=> Something failed!")  # TODO: Capture the smtp logs/error

# \===============================WHITE ZONE IS FOR LOADING AND UNLOADING ONLY=========================================/
# Log our base i
logger.info(i)

# Populate the libs
logger.info("Reading CSV File.")
for row in c:
    given_name.append(row[0])
    samAccountName.append(row[1])
    mail.append(row[2])
    password_status.append(row[3])
    password_expires.append(row[4])
    account_is_disabled.append(row[5])
    account_is_locked_out.append(row[6])
    password_is_expired.append(row[7])

# \====================================================================================================================/

# Do stuff here

# TODO: Check for powershell checkfile
# We need to make sure we're only sending out email if the powershell task
# completed successfully.
ps_completed_check()

# Clean House - get rid of all the old html in the folder.
# clean_house()

# Comment out if testing
# TODO: Add this to config file/cli toggle switch
# sanity_check(given_name, samAccountName, mail, password_status, password_expires, account_is_disabled,
#              account_is_locked_out, password_is_expired, i)

# Nudge out the CSV header, since we're using ii for html creation.
# TODO: We can eliminate this by fixing the powershell output
ii += 1

# This function does all the work. Generates HTML, and sends the messages.
# generate_html_body_and_send(given_name, password_status, ii)



# Get data for a report about what emails were sent or not sent.
total_users_count = len(given_name)-1
texu_len = (len(expired_users))
teu_len = (len(total_exping_users))-texu_len # We do this because the code has expired passwords count as expiring too.

# Send the report
send_report()