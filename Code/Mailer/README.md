# Description and Background #

Requires Swithmail.exe in the html folder : Swithmail can be found here: [https://www.tbare.com/software/swithmail/ ](https://www.tbare.com/software/swithmail/)

This is an application that analyzes the password expiration date of every user, and if that users password is going to expire in 14+1 days, emails them a friendly reminder to change it. 

The drive behind it are the frequent lockouts as a result of neglecting to change it (or ignoring the bubble during login). This also assists in users who are frequently off-site, and may have to make arrangements to come on site and change their password.



- Scheduled task on (DC) starts at 9:38AM (done - tested and working)
	-This task starts a batch file named PS_Report_bootstrap.bat on server (DC)
	-It runs in c:\temp and it starts the password_report.ps1 powershell script
	-The script runs, and creates a csv called IT_minority_report.csv
	-This finishes the task


- At 10:00AM, another scheduled task starts (done - tested and working)
	-This starts a batch file named precog.bat on server (DC)
	-This batch file copies the csv over to c:\mailer
	-It renames the file daily_password.csv


- At 10:12AM, another scheduled task starts (TODO or in-progress tasks are in blue)
	-This task starts a batch file named redball.bat on server (app server)
	-This batchfile bootstraps the mailer.py file
	-	mailer.py
		-	Locates the csv in c:\mailer
		-	Examines the time stamp, and makes sure that it's from today
			-	If not, it aborts, and notifies by email (TODO)
			-	If the file is current, it begins processing it
				-	It removes the expired, locked out, and what not and copies it to a new csv
					-	Added a thing to pass the exception gracefully. Add's text to the email. 
				-	It also removes the first line
					-	Wrote a function to do it on the fly.
				-	Once the CSV is serialized, it examines the csv for soon to expire users
				-	It puts them into 2 buckets two weeks, and 1 week.
				-	It creates the HTML files with their notification, and creates an INI file with a glak flag. (for future scheduling roadmap).
				-	Once finished the script looks for users with a glak flag, and an html file
				-	If it finds one, it takes that information, and creates and sends an email with it.
				-	Then it moves onto the next one, until it's processed all users.
				-	Then it exports a summary, and mails that to the service desk.
				-	It sleeps for 5 minutes
				-	Then it purges all HTML files. 
					-	Wrote a function to do it. And it rotates the logs. 





 
