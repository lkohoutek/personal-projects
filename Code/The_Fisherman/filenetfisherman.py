import os
import smtplib
import email.utils
from email.mime.text import MIMEText
import time
import logging
import shutil
import csv
from bs4 import BeautifulSoup
from ccp.client import Client

# Set date and time variables
current_day = time.strftime("%d")
current_day_day = time.strftime("%a")
current_year = time.strftime("%Y")
current_time = time.strftime("%H:%M")

# Set Current Date
runtime_date = (" Last Checked: " + current_year + " " + current_day + " " + current_day_day + " " + current_time)

# Logging
logging.basicConfig(filename='C:\\1fish\\Fishermans.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.info('>>>=== The Fisherman 0.1! | Mar 18 2015 | lkohoutek@gmail.com         ===<<< ')
logger.info('>>>=== -------------------------------------------------------------  ===<<< ')
logger.info(runtime_date)

# FileNET specifics
# So, the easiest way to do this is to just map the drives prior to running the script. We can write it so if the
# drives are NOT mapped, to error out and send an email. TODO:
#local_dir = "\\\\\"
#local_drive = "F:\\"
#local_error_dir = "\\\\"
#remote_dir = "\\\\"
#remote_drive = "N:\\"
# File Users
#file_user = ""
#file_domain = ""
#file_password = ""
# File Debugging
debug_l_dir = "C:\\1local"
debug_r_dir = "C:\\1remote"
debug_e_dir = "C:\\1error"
debug_c_dir = "C:\\1cash"


# Changelog Stuff
client = Client("changelog server", 5000)

# Main Loop Bit
i = 0

# Set the report to be the date it's ran.
os.chdir(debug_e_dir)
#csv_file_name = "Errors_" + current_day_day + "_" + current_day + "_" + current_year + ".csv"

# open the file for writing.
#c = csv.writer(open(("c:\\erorr\\" + csv_file_name), 'w', newline=''))


# Functions
# TODO: Refactor all of the emailing stuff.

def check_mapdrive():
    try:
        #net_use = ("NET USE O: '%s' %s /USER:%s\%s" % (remote_dir, filenet_password, filenet_domain, filenet_user))
        os.system(debug_l_dir)
    except:
        client.send("Error X", "Error", "Fisherman")


# def unmapdrive():
#     try:
#         # http://stackoverflow.com/questions/2625877/copy-files-to-network-path-or-drive-using-python
#         os.system(r"NET USE O: /DELETE")
#     except:
#         client.send("Error X", "Error", "Fisherman")


def naptime():
    try:
        time.sleep(5)
        logger.info("Sleeping.")
        #client.send("Sleeping for 5 seconds.", "Fisherman")
    except:
        #client.send("Error X","Fisherman")
        logger.error("Sleep failed.")
        pass


def copy_to_cash():
    try:
        # CD into the directory
        os.chdir(debug_l_dir)
        # Set local variable to our cache
        temp = debug_c_dir
        # iterate through all the files
        logger.info("Getting file list:")
        for filename in os.listdir('.'):
            # Log the name of the file we're moving
            logger.info("Moving %s", filename)
            shutil.move(filename, (temp + "\\" + filename))
        # Check Path to temp folder
        # shutil.move local dir to remote dir
        # Log all files touched
    except:
        logger.error("There was an error moving the file")
        # send email notifying an error
        #mail_status_report
        client.send("Error X", "Error", "Fisherman")


def copy_to_remote():
    try:
        # CD into the directory
        os.chdir(debug_c_dir)
        # Set local variable to our cache
        temp = debug_r_dir
        # iterate through all the files
        logger.info("Getting file list:")
        for filename in os.listdir('.'):
            # Log the name of the file we're moving
            logger.info("Moving %s", filename)
            shutil.copy(filename, (temp + "\\" + filename))
        # Check Path to temp folder
        # shutil.move local dir to remote dir
        # Log all files touched
    except:
        logger.error("There was an error moving the file")
        # send email notifying an error
        #mail_status_report
        client.send("Error X", "Error", "Fisherman")


def copy_back_local():
    try:
        # CD into the directory
        os.chdir(debug_r_dir)
        # Set local variable to our cache
        temp = debug_e_dir
        # iterate through all the files
        logger.info("Getting file list:")
        for filename in os.listdir('.'):
            # Log the name of the file we're moving
            logger.info("Moving %s", filename)
            shutil.move(filename, (temp + "\\" + filename))
        # Check Path to temp folder
        # shutil.move local dir to remote dir
        # Log all files touched
    except:
        logger.error("There was an error moving the file")
        # send email notifying an error
        #mail_status_report
        client.send("Error X", "Error", "Fisherman")


def copy_back_debug():
    try:
        # CD into the directory
        os.chdir(debug_e_dir)
        # Set local variable to our cache
        temp = debug_l_dir
        # iterate through all the files
        logger.info("Getting file list:")
        for filename in os.listdir('.'):
            # Log the name of the file we're moving
            logger.info("Moving %s", filename)
            shutil.move(filename, (temp + "\\" + filename))
        # Check Path to temp folder
        # shutil.move local dir to remote dir
        # Log all files touched
    except:
        logger.error("There was an error moving the file")
        # send email notifying an error
        #mail_status_report
        client.send("Error X", "Error", "Fisherman")

# Gets the files in the folder.
def select_files_in_folder(dir, ext):
    for file in os.listdir(dir):
        if file.endswith('.%s' % ext):
            yield os.path.join(dir, file)


# Parse the XML's and get the errors
def get_xml_errors():
    """
    K, so here's what it's doing.

    fileName loads up the full path + file name into a string.
    we use split to chop it up like Reakwon
    We call the 4th position, in our case the filename and toss it into a variable.
    We then Chop it like Jaydee, getting at all the good bits.

    0 : Case Number
    1 : Short Code
    2 : Date
    3 : I forgot
    4: The rest of the crap. File ext, error, and xml.
    etc...etc... See da' header below fo mo info...

    :param targetFolder: This is the share.
    :return:
    """
    try:
        c.writerow(["Case Number", "Short Code", "Date Scanned", "Unknown Value", "File Extension", "Error", "Error Message"])
        for file in select_files_in_folder(debug_r_dir, 'xml'):
            fileName = file.split("\\")[4]
            caseNameParse = fileName.split("_")
            caseNameExt = fileName.split(".")
            soup = BeautifulSoup(open(file))
            # This next bit is ugly, I know.
            # print(caseNameParse[0],",",caseNameParse[1],",",caseNameParse[2],",",caseNameParse[3],",",caseNameExt[1],",",caseNameExt[2],",",soup.get_text().strip(),",")
            c.writerow([caseNameParse[0],caseNameParse[1],caseNameParse[2],caseNameParse[3],caseNameExt[1],caseNameExt[2],soup.get_text().strip()])  # strip protects against newlines...
    except:
        logger.error("Error reading/generating the ErrorNet Report")
        client.send("Error reading/generating the ErrorNet Report", "Error", "Fisherman")
        # Experience is what we get, when we don't get what we want.
        pass


def status_update():
    try:
        client.send("Status Stuff Goes Here", "Info", "Fisherman")
    except:
        logger.error("Failed to post status to Changelog Server")
        updated_body_text = "Failed to post Status to Changelog Server"
        msg = MIMEText(updated_body_text)
        msg['To'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        msg['From'] = email.utils.formataddr(('Author', 'foo@bar.com'))
        msg['Subject'] = "Fisherman's Status"
        server = smtplib.SMTP('smtp')
        try:
            server.sendmail('foo@bar.com', ['foo@bar.com'], msg.as_string())
        finally:
            server.quit()



def mail_error_report():
    try:
        # This is the body text
        status = ["a"]
        status_code = ["b"]
        updated_body_text = ("""

< Fancy Ascii Art Goes Here >

        """).format(date=runtime_date, stat_text=status[0], stat_code=status_code[0])
        msg = MIMEText(updated_body_text)
        msg['To'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        # msg['Cc'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        msg['From'] = email.utils.formataddr(('Author', 'foo@bar.com'))
        msg['Subject'] = "Fisherman's Catch"
        server = smtplib.SMTP('smtp')
        # Uncomment below if relay is being strange:
        # server.set_debuglevel(True) # show communication with the server
        try:
            server.sendmail('foo@bar.com', ['foo@bar.com'], msg.as_string())
        finally:
            server.quit()
        logger.info("Mail should be sent.")
    except:
        logger.error("something failed")  # TODO: Capture the smtp logs/error
        client.send("Failed Sending the Email", "Error", "Fisherman")


def mail_status_report():
    try:
        # This is the body text
        status = ["a"]
        status_code = ["b"]
        updated_body_text = ("""

< Fancy Ascii Art Goes Here >

        """).format(date=runtime_date, stat_text=status[0], stat_code=status_code[0])
        msg = MIMEText(updated_body_text)
        msg['To'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        msg['Cc'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        msg['From'] = email.utils.formataddr(('Author', 'foo@bar.com'))
        msg['Subject'] = "File Fisherman Failed"
        server = smtplib.SMTP('smtp')
        # Uncomment below if relay is being strange:
        # server.set_debuglevel(True) # show communication with the server
        try:
            server.sendmail('foo@bar.com', ['foo@bar.com'], msg.as_string())
        finally:
            server.quit()
        logger.info("Mail should be sent.")
    except:
        logger.error("something failed sending the email")  # TODO: Capture the smtp logs/error
        client.send("Failed Sending the Email", "Error", "Fisherman")


def housekeeping(target):
    try:
        # log contents of target)
        os.chdir(target)
        for filename in os.listdir('.'):
            os.remove(filename)
        # delete(target)
    except:
        client.send("Failed Housekeeping", "Error", "Fisherman")
        logger.error("Housekeeping Failed")


# MAIN LOOP
if __name__ == '__main__':
    while i == 0:
    #os.chdir(local_dir)
        logger.info("Staging.")
        copy_to_cash()
        logger.info("Copy to Remote")
        copy_to_remote()
        logger.info("Bedtime.")
        naptime()
        logger.info("Copying Errors Back.")
        copy_back_local()
        logger.info("Restarting Loop")
        copy_back_debug()
        naptime()
