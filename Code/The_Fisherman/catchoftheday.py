import os
import smtplib
import email.utils
from email.mime.text import MIMEText
import time
import logging
import shutil
import csv
from bs4 import BeautifulSoup
from ccp.client import Client

# Set date and time variables
current_day = time.strftime("%d")
current_day_day = time.strftime("%a")
current_year = time.strftime("%Y")
current_time = time.strftime("%H:%M")

# Set Current Date
runtime_date = (" Last Checked: " + current_year + " " + current_day + " " + current_day_day + " " + current_time)

# Set the report to be the date it's ran.
# os.chdir(debug_e_dir)
csv_file_name = "Errors_" + current_day_day + "_" + current_day + "_" + current_year + ".csv"

# open the file for writing.
c = csv.writer(open(("g:\\temp\\" + csv_file_name), 'w'))


local_drive = "g:\\temp\\"

# Gets the files in the folder.
def select_files_in_folder(dir, ext):
    for file in os.listdir(dir):
        if file.endswith('.%s' % ext):
            yield os.path.join(dir, file)


# Parse the XML's and get the errors
def get_xml_errors():
    """
    K, so here's what it's doing.

    fileName loads up the full path + file name into a string.
    we use split to chop it up like Reakwon
    We call the 4th position, in our case the filename and toss it into a variable.
    We then Chop it like Jaydee, getting at all the good bits.

    0 : Case Number
    1 : Short Code
    2 : Date
    3 : I forgot
    4: The rest of the crap. File ext, error, and xml.
    etc...etc... See da' header below fo' mo' info...

    :param targetFolder: This is the share.
    :return:
    """
    try:
        c.writerow(["File Name", "Error Message"])
        for file in select_files_in_folder(local_drive, 'xml'):
            #print(file)
            file_name = file.split("\\")[2]
            #print(file_name)
            case_name_parse = file_name.split("_")
            case_name_ext = file_name.split(".")
            soup = BeautifulSoup(open(file))
            # This next bit is ugly, I know.
            print([file_name,soup.get_text().strip()])
            c.writerow([file_name,soup.get_text().strip()])  # strip protects against newlines...
    except:
        print("shit")


get_xml_errors()