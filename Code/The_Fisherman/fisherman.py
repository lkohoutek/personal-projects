#!/usr/bin/env python
# title           : The Fisherman
# description     : Manipulates files.
# author          : Levon Kohoutek | lkohoutek@gmail.com
# date            : 20150519
# version         : 0.1.2
# usage           : python fisherman.py
# python_version  : 3.4.x
# notes           : Pretty simple stuff going on here.
#
# ==============================================================================

# TODO: Refactor all of the emailing stuff.
# TODO: Finish Implementing Drive Mapping
# TODO: Fix CSV Error Report
# TODO: Fix Changelog Server Messages
# TODO: Keep Count of how many files were copied, and report on it
# TODO: Finish Refactoring things down into configuration ini file

import os
import smtplib
import email.utils
from email.mime.text import MIMEText
import time
import logging
import shutil
# from ccp.client import Client
import configparser

# Set date and time variables
current_day = time.strftime("%d")
current_day_day = time.strftime("%a")
current_year = time.strftime("%Y")
current_time = time.strftime("%H:%M")

# Set Current Date
runtime_date = (" Last Checked: " + current_year + " " + current_day + " " + current_day_day + " " + current_time)

# Logging
logging.basicConfig(filename='M:\\The_Fisherman\\Fishermans.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.info('>>>=== The Fisherman 0.1.2! | May 18 2015 | lkohoutek@gmail.com        ===<<< ')
logger.info('>>>=== Last updated: May 27 2015. News: We have an ini file now!       ===<<< ')
logger.info('>>>=== --------------------------------------------------------------  ===<<< ')
logger.info(runtime_date)

# Initial livewell.ini Configuration Loader
config = configparser.ConfigParser()
config_filename = "livewell.ini"
config.read(config_filename)

# Set sleep time
sleep_for_time = config['Sleep']['sleeptime']


# File specifics
# So, the easiest way to do this is to just map the drives prior to running the script. We can write it so if the
# drives are NOT mapped, to error out and send an email. TODO:
local_dir = config['FileNet']['local_dir']
local_drive = config['FileNet']['local_drive']
local_error_dir = config['FileNet']['local_error_dir']
local_error_drive = config['FileNet']['local_error_dir']
local_cache = config['FileNet']['local_cache']
local_app = config['FileNet']['local_app']
remote_dir = config['FileNet']['remote_dir']
remote_drive = config['FileNet']['remote_drive']

# Filenet Users
filenet_user = config['FileNet']['filenet_user']
filenet_domain = config['FileNet']['filenet_domain']
filenet_password = config['FileNet']['filenet_password']

# Filenet Debugging
debug_l_dir = config['FileNet']['debug_l_dir']
debug_r_dir = config['FileNet']['debug_r_dir']
debug_e_dir = config['FileNet']['debug_e_dir']
debug_c_dir = config['FileNet']['debug_c_dir']

# Changelog Stuff
#cl_serv = config['ChangeLog']['cl_serv']
#cl_smooth = config['ChangeLog']['cl_smooth'] # They reminisce...
#client = Client(cl_serv, (int(cl_smooth)))

# Main Loop Bit
# We going to make this in the ini, so you can stop it clean.
stop_bit = config['Ops']['shutdown']
i = (int(stop_bit))
# i = 0


# Functions
def contraband_check():
    try:
        os.chdir(local_app)
        if os.path.exists("contraband.ini") == True:
            logger.error("Dirty Bit is flipped. Something bad happened and it's jacked up now.")
            mail_error_report("The Fisherman is dirty. Check your cargo.")
            exit()
        else:
            logger.info("Fisherman is clear to go fishing!")
            pass
    except:
        pass


def dirty_bit_check(conf_name):
    try:
        config = configparser.ConfigParser()
        config_filename = conf_name
        config.read(config_filename)
        dirty_bit = config['Bit']['dirtybit']
        i = (int(dirty_bit))
        if i == 1:
            logger.error("Dirty Bit is flipped. Something bad happened and it's jacked up now.")
            mail_error_report("The Fisherman is dirty. Check your cargo.")
            exit()
        else:
            logger.info("Fisherman is clear to go fishing!")
            pass
    except:
        logger.error("Unable to evaluate the dirty bit.")
        mail_error_report("The Fisherman is dirty. Check your cargo.")
        exit()


def dirty_bit_flip():
    try:
        os.chdir(local_app)
        contraband = open('contraband.ini','w')
        config.add_section('DirtyBit')
        config.set('DirtyBit', 'bit', '1')
        config.write(contraband)
        contraband.close()
    except:
        logger.error("Unable to evaluate the dirty bit.")
        mail_error_report("The Fisherman is dirty. Check your cargo.")
        exit()


def mapdrive():
    try:
        net_use = ("NET USE O: '%s' %s /USER:%s\%s" % (remote_dir, filenet_password, filenet_domain, filenet_user))
        os.system(net_use)
    except OSError:
        logger.error("Mapping the drive Failed")
        pass


def unmapdrive():
    try:
        os.system(r"NET USE O: /DELETE")
    except OSError:
        logger.error("Disconnecting the drive Failed")


def naptime():
    try:
        time.sleep(int(sleep_for_time))  # This is in seconds.
        logger.info("Sleeping.")
    except Exception:
        logger.error("Sleep failed.")
        pass


def file_mover(src, dst):
    try:
        # CD into the directory with the files.
        os.chdir(src)
        # Set destination.
        to_dir = dst
        # Check if the dest dir exists (note: if src fails, it excepts out to the same error).
        if os.path.exists(to_dir):
            # iterate through all the files in the src.
            logger.info("Getting file list:")
            number_of_files_touched = 0
            for filename in os.listdir('.'):
                number_of_files_touched += 1
                try:
                    # Log the name of the file we're moving.
                    logger.info("Moving %s from %s to %s", filename, src, dst)
                    shutil.move(filename, (to_dir + "\\" + filename))
                except IOError:
                    logger.error("A File is locked: %s", filename)
            logger.info("Moved %s files.", str(number_of_files_touched))
        # If path does not exist, raise error.
        else:
            raise IsADirectoryError
    # If file is locked, log it, and move on.
    except IOError:
        logger.error("A File is locked.")
        pass
    # If at any point the copy runs into a problem, abort, and alert.
    except IsADirectoryError:
        dirty_bit_flip()
        logger.error("There was an error moving the file")
        mail_error_report("File Mover threw an error. Check Directory.")
        exit()


def file_copier(src, dst):
    # This is the same as the file_mover function above, however this one copies.
    try:
        os.chdir(src)
        to_dir = dst
        if os.path.exists(to_dir):
            logger.info("Getting file list:")
            number_of_files_touched = 0
            for filename in os.listdir('.'):
                number_of_files_touched += 1
                logger.info("Copying %s from %s to %s", filename, src, dst)
                shutil.copy2(filename, (to_dir + "\\" + filename))
            logger.info("Copied %s files.", str(number_of_files_touched))
        else:
            raise IsADirectoryError
    except IsADirectoryError:
        dirty_bit_flip()
        logger.error("There was an error moving the file")
        mail_error_report("File Mover threw an error. Check Directory.")
        exit()


def status_update():
    # For changelog Server
    try:
        # client.send("Status Stuff Goes Here", "Info", "Fisherman")
        pass
    except KeyError:
        logger.error("Failed to post status to Changelog Server")
        updated_body_text = "Failed to post Status to Changelog Server"
        msg = MIMEText(updated_body_text)
        msg['To'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        msg['From'] = email.utils.formataddr(('Author', 'foo@bar.com'))
        msg['Subject'] = "Fisherman's Status"
        server = smtplib.SMTP('smtp')
        try:
            server.sendmail('foo@bar.com', ['foo@bar.com'], msg.as_string())
        finally:
            server.quit()


def mail_error_report(message):
    try:
        # This is the body text
        status = message
        status_code = ["b"]
        updated_body_text = ("""A'hoy! Rough Seas. Alert:

        {stat_text}

< Fancy Ascii Art Goes Here >

        """).format(date=runtime_date, stat_text=status, stat_code=status_code[0])
        msg = MIMEText(updated_body_text)
        msg['To'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        # msg['Cc'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        msg['From'] = email.utils.formataddr(('Author', 'foo@bar.com'))
        msg['Subject'] = "The Fisherman sends an SOS!"
        server = smtplib.SMTP('smtp')
        # Uncomment below if relay is being strange:
        # server.set_debuglevel(True) # show communication with the server
        try:
            server.sendmail('foo@bar.com', ['foo@bar.com'], msg.as_string())
        finally:
            server.quit()
        logger.info("Mail should be sent.")
    except Exception:
        logger.error("something failed")  # TODO: Capture the smtp logs/error


def housekeeping(target):
    try:
        os.chdir(target)
        if os.getcwd() == target:
            for filename in os.listdir('.'):
                logger.info("Deleting: %s from %s", filename, target)
                os.remove(filename)
        else:
            dirty_bit_flip()
            logger.error("Housekeeping Failed - Fix and Manually Empty the Cache before restarting.")
            mail_error_report("Housekeeping Failed - Fix and Manually Empty the Cache before restarting.")
            raise Exception("Failed to verify folder with files to be deleted.")
    except FileNotFoundError:
        dirty_bit_flip()
        logger.error("Housekeeping Failed")
        mail_error_report("Housekeeping has failed!")
        exit()


# MAIN LOOP
if __name__ == '__main__':
    try:
        logger.info(runtime_date)
        contraband_check()
        os.chdir(local_dir)  # Homebase
        logger.info("Staging.")
        file_mover(local_drive, local_cache)  # Move from Local to Cache.
        logger.info("Copy to Remote")
        file_copier(local_cache, remote_drive)  # Copy from Cache to Remote.
        logger.info("Bedtime.")
        print("Sleeping for %s seconds." % sleep_for_time)  # Left here for Console Debugging/Exit.
        naptime()  # Needs to be less than the time the Scheduled Task is.
        print("Awake.")
        logger.info("Housekeeping!")
        housekeeping(local_cache)  # Empty Cache.
        logger.info("Copying Errors Back.")
        file_mover(remote_drive, local_error_drive)  # Move file from Remote to Error.
        logger.info("Preparing to Stage.")
    except KeyboardInterrupt:
        print("Yer' exiting, eh?")
        time.sleep(10)
        logger.info("Someone Control C'd out this script at: %s" % runtime_date)
        dirty_bit_flip()
        exit()