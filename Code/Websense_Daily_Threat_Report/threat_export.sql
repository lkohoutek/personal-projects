-- lkohoutek@gmail.com | 4-8-2015
-- 
-- This returns the last 24 hours of Suspicious Activity in Websense. 
-- There isn't much of an easier way.  
--
-- Notes: 
-- http://stackoverflow.com/questions/15631977/subtract-one-day-from-datetime
-- http://www.websense.com/content/support/library/web/v77/reporting_faq/export_threats.aspx
--

select * from amt_UI_log_details where date_time between (SELECT DATEADD(day,-1,(select getdate( )))) AND (select getdate( ))
