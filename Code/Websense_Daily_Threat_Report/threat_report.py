# Threat Report Email
# lkohoutek@gmail.com | 4.8.2015


import os
import logging
import time
import smtplib
import email.utils
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart


# Set date and time variables
current_day = time.strftime("%d")
current_day_day = time.strftime("%a")
current_year = time.strftime("%Y")
current_time = time.strftime("%H:%M")

runtime_date = (current_year + " " + current_day + " " + current_day_day + " " + current_time)

# 
os.chdir('C:\\Websense_Reports')

# Logging Stuff
# We need to add a flag to set how verbose we want to log.
logging.basicConfig(filename='Threat.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.info('>>>=== Threat Report 0.0.1 | Mar 3 2015 | foo@bar.com                   ===<<< ')
logger.info('>>>=== ---------------------------------------------------------------  ===<<< ')
logger.info(runtime_date)

def send_report():
    """
    This is the basic boilerplate I've been using to send emails. I'm sure there are better ways, but this issimple,
     and it works.

    :return:
    """
    try:
        runtime_date = (current_year + " " + current_day + " " +
                        current_day_day + " " + current_time)
        updated_body_text = ("""Daily Threat Report: {date}

24 Hour Threat Report.

        """).format(date=runtime_date)
        msg = MIMEMultipart()
        msg['To'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        # Uncomment below to add additional recipients.
        # msg['Cc'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        msg['From'] = email.utils.formataddr(('Author', 'foo@bar.com'))
        msg['Subject'] = "Daily Threat Report"
        msg.attach(MIMEText(updated_body_text))
        server = smtplib.SMTP('smtp')
        # Uncomment below if relay is being strange:
        # server.set_debuglevel(True) # show communication with the server
        os.chdir("C:\\Websense_Reports")
        f = "Daily_Threats.csv"
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(f, 'rb').read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="Daily_Threats.csv"')
        msg.attach(part)
        try:
            server.sendmail('foo@bar.com', ['foo@bar.com'], msg.as_string())
            logger.info("=> Sent.")
            pass
        finally:
            server.quit()
    except:
        logger.error("=> Email failed!")  # TODO: Capture the smtp logs/error


# Main
send_report()
logger.info("=> Completed.")
