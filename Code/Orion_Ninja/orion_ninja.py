#!/usr/bin/env python
# title           : Orion_Ninja.py
# description     : Scrape the JSON from Orion.
# author          : Levon Kohoutek | lkohoutek@gmail.com
# date            : 20150304
#  version         :0.1
# usage           : python orion_ninja.py
# python_version  : 3.4
# notes           : Pretty simple stuff going on here.
#
# ==============================================================================

import requests
import json
from requests.auth import HTTPBasicAuth
# from pprint import *
from orion_schemas import *

# Service Account User
pw = "**Orion Service Account Password**"
user = "orionapi"

# See Doc on Schemas
# Maybe offload these to orion_schemas.py

# Set Up the session
s = requests.Session()
s.auth = HTTPBasicAuth(user, pw)

# Update the Header
s.headers.update({"Content-Type": "application/json"})

# Set blank Payload
payload = {"query": ""}


# /========================================= FUNCTIONS ================================================================|

def alert_status():
    """
    There's an easier way to do this. For now, I'm wrapping payloads in functions named after the query.
    TODO: abstract it more and put the query's in their own file.
    :return:
    """
    the_data = []  # Local temp store for the data we're working with.
    payload['query'] = 'SELECT AlertDefID, ActiveObject, ObjectType, State, WorkingState, ObjectName, AlertMessage, ' \
                       'TriggerTimeStamp, TriggerTimeOffset, TriggerCount, ' \
                       'ResetTimeStamp, Acknowledged, AcknowledgedBy, AcknowledgedTime, LastUpdate,' \
                       ' AlertNotes, Notes, AlertObjectID FROM Orion.AlertStatus'
    r = s.post("https://**Your Server**:17778/SolarWinds/InformationService/v3/Json/Query",
               data=json.dumps(payload), verify=False)
    the_data.append(r.json())
    d = the_data[0]['results']
    for x in d:
        as_ObjectName.append((x['ObjectName']))  # This for example appends the object name that's alerting.
    print(len(as_ObjectName))  # Counts how many objects are alerting, and returns the number.


def nodes():
    """
    There's an easier way to do this. For now, I'm wrapping payloads in functions named after the query.
    TODO: abstract it more and put the query's in their own file.
    :return:
    """
    the_data = []  # Local temp store for the data we're working with.
    payload['query'] = 'SELECT NodeID, ObjectSubType, IPAddress, IPAddressType, DynamicIP, Caption, NodeDescription, ' \
                       'Description, DNS, SysName, Vendor, SysObjectID, Location, Contact, VendorIcon, Icon, Status, ' \
                       'StatusLED, StatusDescription, CustomStatus, IOSImage, IOSVersion, GroupStatus, StatusIcon, ' \
                       'LastBoot, SystemUpTime, ResponseTime, PercentLoss, AvgResponseTime, MinResponseTime, ' \
                       'MaxResponseTime, CPULoad, MemoryUsed, MemoryAvailable, PercentMemoryUsed, ' \
                       'PercentMemoryAvailable, LastSync, LastSystemUpTimePollUtc, MachineType, IsServer, Severity,' \
                       ' UiSeverity, ChildStatus, Allow64BitCounters, AgentPort, TotalMemory, CMTS, ' \
                       'CustomPollerLastStatisticsPoll, CustomPollerLastStatisticsPollSuccess, SNMPVersion, ' \
                       'PollInterval, EngineID, RediscoveryInterval, NextPoll, NextRediscovery, StatCollection, ' \
                       'External, Community, RWCommunity, IP, IP_Address, IPAddressGUID, NodeName, BlockUntil, ' \
                       'BufferNoMemThisHour, BufferNoMemToday, BufferSmMissThisHour, BufferSmMissToday, ' \
                       'BufferMdMissThisHour, BufferMdMissToday, BufferBgMissThisHour, BufferBgMissToday, ' \
                       'BufferLgMissThisHour, BufferLgMissToday, BufferHgMissThisHour, BufferHgMissToday, ' \
                       'OrionIdPrefix, OrionIdColumn, SkippedPollingCycles, MinutesSinceLastSync, EntityType, ' \
                       'DetailsUrl, DisplayName FROM Orion.Nodes'
    r = s.post("https://**Your Server**:17778/SolarWinds/InformationService/v3/Json/Query",
               data=json.dumps(payload), verify=False)
    the_data.append(r.json())
    d = the_data[0]['results']
    for x in d:
        node_NodeName.append((x['NodeName']))  # This for example, this appends the node's name to a dict.
    print(len(node_NodeName))  # Counts how many nodes we're monitoring, and returns the number.


# /==================================== DO STUFF ======================================================================|

alert_status()
nodes()
