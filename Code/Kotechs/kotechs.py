# Kotechs - Your daily notepad refresh!
# kohoutekl (at) gmail (dot) com | 4.5.2017
import os
import time
import psutil
import random
import traceback
from pywinauto.application import Application

# Set date and time variables
current_day = time.strftime("%d")
current_day_day = time.strftime("%a")
current_year = time.strftime("%Y")
current_time = time.strftime("%H:%M")
current_month = time.strftime("%m")

# Hold the pids
open_pads = []

# Hold the name
autogen_name = []

# Base dir
base_dir = r"C:\kotechs"
folder_name = current_year + "_" + current_month + "_" + current_day + "_" + current_day_day
the_full_path = base_dir + "\\" + folder_name


def file_name_creator():
    try:
        autogen_name.clear()
        # Filename = Something Hipsters like + Weird Wisconsin Town Name + Random number between 1 and 100
        prefix = ['VHS', 'PABST', 'Schlitz', 'Hexagon', 'Chia', 'Plaid', 'Mixtape', 'Fingerstache']
        middle = ['Oconomowoc', 'ClamFalls', 'EggHarbor', 'Manitowoc', 'Kewaunee', 'Herbster', 'Peshtigo', 'Weyauwega']
        suffix = random.randrange(1, 100)
        the_prefix = random.choice(prefix)
        the_middle = random.choice(middle)
        the_name = the_prefix + "_" + the_middle + "_" + str(suffix)
        autogen_name.append(the_name)
    except:
        traceback.print_exc()


def enta_da_stage():
    try:
        # Check directories
        if os.path.isdir(the_full_path) is True:
            print("Directory already exists.")
        elif os.path.isdir(the_full_path) is False:
            os.mkdir(the_full_path)
            if os.path.isdir(the_full_path) is True:
                print("Directory did not exist. Created the directory.")
        else:
            print("Something weird has happened. Check the base directory.")
            raise Exception
        print("Will be saving to: " + str(the_full_path))
        # Get all Notepad2 PIDs
        procname = "Notepad2.exe"
        for proc in psutil.process_iter():
            if proc.name() == procname:
                open_pads.append(proc.pid)
        if len(open_pads) is 0:
            print("No running instances of Notepad2.exe were found. Nothing to save.")
        elif len(open_pads) > 0:
            pass
    except:
        traceback.print_exc()


def save_all_notepads():
    try:
        for i in open_pads:
            print("Trying to connect to: " + str(i))
            app = Application().connect(process=i)
            dlg = app['* Untitled - Notepad2']
            dlg.MenuSelect("File->Save As...")
            file_name_creator()
            full_file_name = (the_full_path + "\\" + autogen_name[0] + ".txt")
            app.SaveAs.Edit.SetEditText(full_file_name)
            app.SaveAs.Save.CloseClick()
            print("Saved file: " + full_file_name)
            new_dlg = autogen_name[0] + ".txt" + " - Notepad2"
            time.sleep(2)  # Giving it a moment to refresh
            dlg = app[new_dlg]
            dlg.MenuSelect("File->Exit")
    except:
        traceback.print_exc()


# Main
enta_da_stage()
save_all_notepads()
