#!/usr/bin/env python
#title           : Esse.py
#description     : Check on the Domain Controller & Firewall Logging Databases.
#author          : Levon Kohoutek | lkohoutek@gmail.com
#date            : 20150114
#version         : 0.2
#usage           : python esse.py
#python_version  : 3.4
#notes           : Pretty simple stuff going on here.
# TODO: Exception Handling! Move ES server addresses out of functions, dude! C'mon!
#
##==============================================================================

import requests
import smtplib
import email.utils
from email.mime.text import MIMEText
import time
# See: https://github.com/woohgit/changelog-client-python
from ccp.client import Client
client = Client("Changelog Server", 5000)


active_primary_shards = []
number_of_nodes = []
initializing_shards = []
number_of_data_nodes = []
timed_out = []
status = []
active_shards = []
relocating_shards = []
cluster_name = []
unassigned_shards = []

# Set date and time variables
current_day = time.strftime("%d")
current_day_day = time.strftime("%a")
current_year = time.strftime("%Y")
current_time = time.strftime("%H:%M")


def get_es_clu_health():
    try:
        # Check Status - Pull JSON.
        try:
            dc_health = requests.get('http://**ES cluster**:9200/_cluster/health')
            dc_clu_stats = dc_health.json()
            # Start Domain Controllers
            active_primary_shards.append(str(dc_clu_stats.get("active_primary_shards")))
            number_of_nodes.append(str(dc_clu_stats.get("number_of_nodes")))
            initializing_shards.append(str(dc_clu_stats.get("initializing_shards")))
            number_of_data_nodes.append(str(dc_clu_stats.get("number_of_data_nodes")))
            timed_out.append(str(dc_clu_stats.get("timed_out")))
            status.append(str(dc_clu_stats.get("status")))
            active_shards.append(str(dc_clu_stats.get("active_shards")))
            relocating_shards.append(str(dc_clu_stats.get("relocating_shards")))
            cluster_name.append(dc_clu_stats.get("cluster_name"))
            unassigned_shards.append(str(dc_clu_stats.get("unassigned_shards")))
        except:
            print('Failed to get DC Status')
            active_primary_shards.append("OFFLINE")
            number_of_nodes.append("OFFLINE")
            initializing_shards.append("OFFLINE")
            number_of_data_nodes.append("OFFLINE")
            timed_out.append("OFFLINE")
            status.append("OFFLINE")
            active_shards.append("OFFLINE")
            relocating_shards.append("OFFLINE")
            cluster_name.append("OFFLINE")
            unassigned_shards.append("OFFLINE")
        try:
            fw_health = requests.get('http://**ES Cluster**:9200/_cluster/health')
            fw_clu_stats = fw_health.json()
                    # Start Firewall
            active_primary_shards.append(str(fw_clu_stats.get("active_primary_shards")))
            number_of_nodes.append(str(fw_clu_stats.get("number_of_nodes")))
            initializing_shards.append(str(fw_clu_stats.get("initializing_shards")))
            number_of_data_nodes.append(str(fw_clu_stats.get("number_of_data_nodes")))
            timed_out.append(str(fw_clu_stats.get("timed_out")))
            status.append(str(fw_clu_stats.get("status")))
            active_shards.append(str(fw_clu_stats.get("active_shards")))
            relocating_shards.append(str(fw_clu_stats.get("relocating_shards")))
            cluster_name.append(fw_clu_stats.get("cluster_name"))
            unassigned_shards.append(str(fw_clu_stats.get("unassigned_shards")))
        except:
            active_primary_shards.append("OFFLINE")
            number_of_nodes.append("OFFLINE")
            initializing_shards.append("OFFLINE")
            number_of_data_nodes.append("OFFLINE")
            timed_out.append("OFFLINE")
            status.append("OFFLINE")
            active_shards.append("OFFLINE")
            relocating_shards.append("OFFLINE")
            cluster_name.append("OFFLINE")
            unassigned_shards.append("OFFLINE")
    except:
        print("Unable To Get ES Status")  # TODO: Send alert via email if it fails!

# Refactor get_es_clu_health
# def get_es_clu_health(cluster):
#     try:
#        # Check Status - Pull JSON.
#        clu_health = requests.get(cluster)
#        es_clu_stats = clu_health.json()
#        active_primary_shards.append(str(es_clu_stats.get("active_primary_shards")))
#        number_of_nodes.append(str(es_clu_stats.get("number_of_nodes")))
#        initializing_shards.append(str(es_clu_stats.get("initializing_shards")))
#        number_of_data_nodes.append(str(es_clu_stats.get("number_of_data_nodes")))
#        timed_out.append(str(es_clu_stats.get("timed_out")))
#        status.append(str(es_clu_stats.get("status")))
#        active_shards.append(str(es_clu_stats.get("active_shards")))
#        relocating_shards.append(str(es_clu_stats.get("relocating_shards")))
#        cluster_name.append(es_clu_stats.get("cluster_name"))
#        unassigned_shards.append(str(es_clu_stats.get("unassigned_shards")))
#    except:
#        print("Unable To Get ES Status")  # TODO: Send alert via email if it fails!
#

# Dump Keys from the ES Responses if we need them
def get_keys(json_object):
    for key in json_object.keys():
        print("key: %s, value: %s" % (key, json_object[key]))


# Send the status vis email
def mail_status_report():
    try:
        # This is the body text to use when pulling the Cluster Health
        runtime_date = (" Last Checked: " + current_year + " " + current_day + " " +
                        current_day_day + " " + current_time)
        updated_body_text = ("""Elastic Search Shard Report: {date}

=== Server: **ES Server(s)** | Type: Domain ==============

Cluster Name: {dc_cn}
DC Overall Status: {dc_s}

DC Active Shards: {dc_as}
DC Initializing Shards: {dc_as}
DC Unassigned Shards: {dc_us}


=== Server: **ES Server(s)**  | Type: Firewall ===========

Cluster Name: {fw_cn}
FW Overall Status: {fw_s}

FW Active Shards: {fw_as}
FW Initializing Shards: {fw_is}
FW Unassigned Shards: {fw_us}
        """).format(date=runtime_date, dc_cn=cluster_name[0], dc_s=status[0], dc_as=active_primary_shards[0],
                    dc_is=initializing_shards[0], dc_us=unassigned_shards[0], fw_cn=cluster_name[1], fw_s=status[1],
                    fw_as=active_primary_shards[1], fw_is=initializing_shards[1], fw_us=unassigned_shards[1])
        msg = MIMEText(updated_body_text)
        msg['To'] = email.utils.formataddr(('Recipient', 'foo@bar.com'))
        msg['From'] = email.utils.formataddr(('Author', 'foo@bar.com'))
        msg['Subject'] = "ES Status Report"
        server = smtplib.SMTP('smtp')
        # Uncomment below if relay is being strange:
        # server.set_debuglevel(True) # show communication with the server
        try:
            server.sendmail('foo@bar.com', ['foo@bar.com'], msg.as_string())
        finally:
            server.quit()
    except:
        print("something failed")  # TODO: Capture the smtp logs/error

def update_changelog():
    try:
        message = "Logstash Cluster Status: 1)Firewall: " + status[1] + ". 2)Domain Controllers: " + status[0] + "."
        client.send(message, 1, "#Logstash")
        pass
    except:
        client.send("Status has failed to be gotten!", 5, "#Logstash")
        pass


get_es_clu_health()
mail_status_report()
update_changelog()