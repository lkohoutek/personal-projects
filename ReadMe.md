# Personal Projects #

A collection of random scripts I've written in the last year of learning of Python. 

All these projects are in 3.x. 

Questions, Complaints, etc.. : lkohoutek(at)gmail(dot)com.


## Projects ##

- The Fisherman
- Account Mailer
- Orion Ninja
- more to come...


## Environment ##
>pip freeze

- Babel==1.3
- Envelopes==0.4
- Flask==0.10.1
- Flask-Admin==1.0.9
- Flask-Babel==0.9
- Flask-Login==0.2.11
- Flask-Mail==0.9.1
- Flask-OpenID==1.2.4
- Flask-Principal==0.4.0
- Flask-SQLAlchemy==2.0
- Flask-Security==1.7.4
- Flask-WTF==0.11
- Flask-WhooshAlchemy==0.56
- Jinja2==2.7.3
- MarkupSafe==0.23
- PySimpleSOAP==1.10
- PyYAML==3.11
- SQLAlchemy==0.9.8
- SimpleAES==0.0.0
- WTForms==2.0.2
- Werkzeug==0.10.1
- Whoosh==2.6.0
- argh==0.25.0
- beautifulsoup4==4.3.2
- blinker==1.3
- ccp==1.0
- certifi==14.05.14
- cffi==0.9.2
- coverage==3.7.1
- cryptography==0.8.1
- cx-Freeze==4.3.3
- defusedxml==0.4.1
- docopt==0.6.2
- easywatch==0.0.3
- elasticsearch==1.2.0
- elasticsearch-dsl==0.0.2
- flipflop==1.0
- guess-language==0.2
- guess-language-spirit==0.5.1
- ipython==2.2.0
- itsdangerous==0.24
- oauthlib==0.7.2
- onedrivesdk==1.0.1
- outbox==0.1.8
- passlib==1.6.2
- pathtools==0.1.2
- peewee==2.6.0
- pyasn1==0.1.7
- pycparser==2.10
- pycrypto==2.6.1
- pypyodbc==1.3.3
- pyreadline==2.0
- python-dateutil==2.3
- python-ntlm3==1.0.2
- python3-openid==3.0.5
- pytz==2014.10
- pyzmq==14.3.1
- redis==2.10.3
- requests==2.8.1
- requests-ntlm==0.1.0
- requests-oauthlib==0.4.2
- selenium==2.42.1
- sharepoint==0.4.1
- simplejson==3.6.4
- six==1.8.0
- speaklater==1.3
- splinter==0.6.0
- staticjinja==0.3.0
- tornado==4.0.1
- urllib3==1.9.1
- watchdog==0.8.0